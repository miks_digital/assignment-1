<?php

	// Night time start and end
	$settingsNightTimeStart  = '22:00';
	$settingsNightTimeEnd    = '07:00';
	
	// Employees & work schedule
	$employeeWorkSchedule = array(
		
		'0' => array(
			'name'       => 'Steven M. Huddleston',
			'shiftStart' => '15:15',
			'shiftEnd'   => '20:45'
		),
		
		'1' => array(
			'name'       => 'Thomas J. Gordon',
			'shiftStart' => '10:00',
			'shiftEnd'   => '22:00'
		),
		
		'2' => array(
			'name'       => 'Ashley T. Chambers',
			'shiftStart' => '22:30',
			'shiftEnd'   => '08:00'
		),
		
		'3' => array(
			'name'       => 'Polly E. Barrow',
			'shiftStart' => '20:00',
			'shiftEnd'   => '10:00'
		),
		
		'4' => array(
			'name'       => 'Jason C. Swanson',
			'shiftStart' => '09:00',
			'shiftEnd'   => '17:00'
		),
		
		'5' => array(
			'name'       => 'Christian S. Carrier',
			'shiftStart' => '23:00',
			'shiftEnd'   => '06:00'
		),
		
	);

    // calculate and set times into employee data array to have it in one place
	foreach ($employeeWorkSchedule as $key => $emplData) {
	    try {
            $times = convertTimes($emplData);
        } catch (Exception $e) {
            echo 'Exception: ',  $e->getMessage(), "\n";
        }
	    $employeeWorkSchedule[$key]['shift_length'] = getShiftLength($times['shiftStart'], $times['shiftEnd']);
	    $employeeWorkSchedule[$key]['night_hours']  = calculateNightHours(
	        $times['shiftStart'],
            $times['shiftEnd'],
            $settingsNightTimeStart,
            $settingsNightTimeEnd
        );
	    $employeeWorkSchedule[$key]['day_hours'] = $employeeWorkSchedule[$key]['shift_length'] - $employeeWorkSchedule[$key]['night_hours'];
	}

	print(getOutput($employeeWorkSchedule));

    /**
    * @param array $emplData
    * @return string
    */
	function getOutput(array $emplData = [])
    {
	    $output = "";

        foreach ($emplData as $info) {
            $calculatedShiftLength = calculateTime($info["shift_length"]);
            $calculatedDayTimes   = calculateTime($info["day_hours"]);
            $calculatedNightTimes = calculateTime($info["night_hours"]);
            $output .= sprintf(
                "Name: %s; Shift start: %s; Shift end: %s; Shift length: %d.%d; Day hours: %d.%d; Night hours: %d.%d",
                    $info["name"],
                    $info["shiftStart"],
                    $info["shiftEnd"],
                    $calculatedShiftLength['h'],
                    $calculatedShiftLength['m'],
                    $calculatedDayTimes['h'],
                    $calculatedDayTimes['m'],
                    $calculatedNightTimes['h'],
                    $calculatedNightTimes['m']
                ) . "\n";
        }
        return $output;
    }

    /**
    * @param string $shiftStart
    * @param string $shiftEnd
    * @param string $settingsNightTimeStart
    * @param string $settingsNightTimeEnd
    * @return int
    */
	function calculateNightHours($shiftStart, $shiftEnd, $settingsNightTimeStart = '', $settingsNightTimeEnd = '')
    {
        if ($shiftStart < $shiftEnd) {
            $start_to_end = range($shiftStart,$shiftEnd,1);
        } else {
            // overnight shift
            $start_to_end = range($shiftStart,strtotime('+1 day',$shiftEnd),1);
        }

        if($settingsNightTimeStart < $settingsNightTimeEnd) {
            $night_hrs = range(strtotime($settingsNightTimeStart),strtotime($settingsNightTimeEnd),1);
        } else {
            // overnight shift
            $night_hrs = range(strtotime($settingsNightTimeStart),strtotime('+1 day', strtotime($settingsNightTimeEnd)),1);
        }

        // as times are whole integer seconds sequence, we can imagine it as a two vectors at time line
        $overlap = array_intersect($start_to_end,$night_hrs);
        return empty($overlap) ? 0 : count($overlap)-1;
    }

    /**
    * @param string $shiftStart
    * @param string $shiftEnd
    * @return int , seconds
    */
    function getShiftLength($shiftStart, $shiftEnd)
    {
        if ($shiftStart > $shiftEnd) {
            // overnight shift
            $shiftEnd = strtotime('+1 day', $shiftEnd);
        }

        return $shiftEnd - $shiftStart;  // in seconds
    }

    /**
    * @param $inputSeconds
    * @return array
    */
    function calculateTime($inputSeconds)
    {
        $secondsInAMinute = 60;
        $secondsInAnHour  = 60 * $secondsInAMinute;
        $secondsInADay    = 24 * $secondsInAnHour;

        // extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);

        // extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);

        return [
            'h' => (int)$hours,
            'm' => (int)$minutes
        ];
    }

    /**
    * @param array $emplData
    * @return array , times in seconds
    * @throws Exception
    */
    function convertTimes(array $emplData = [])
    {
        $accuracy_minutes = 15;
        $accuracy = $accuracy_minutes * 60;
        $shiftStart = strtotime($emplData['shiftStart']);
        $shiftEnd   = strtotime($emplData['shiftEnd']);

        if(!$shiftStart || !$shiftEnd || ($shiftStart % $accuracy != 0) || ($shiftEnd % $accuracy != 0)) {
            $message = sprintf('Wrong time input accuracy for employee %s', $emplData['name']);
            throw new Exception($message);
        }

        return [
            'shiftStart' => $shiftStart,
            'shiftEnd'   => $shiftEnd
        ];
    }
?>