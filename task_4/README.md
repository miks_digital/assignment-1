As far as I can see, we are talking about storing and fetching data from RDBMS SQL database.
That means table-based data structure with strict predefined schemas.
NoSQL (Not Only SQL or non-SQL) can be document based, graph databases or key-value pairs.
NoSQL databases don’t require any predefined schema, allowing you to work more freely with "unstructured data". 

So depending on the current situation and looking to the future I would propose to store more complex custom products as
JSON data in SQL table or use NoSQL database.
