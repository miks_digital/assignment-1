To insert missing rows we need to know diff between what we have now and what we had in the old system.
In case if there is no primary key to check in new system against old one, the only way to get diff is to go through each 
row in new system and compare its data to each record in old system until we get missing row(s) to save in new system.

To prevent such a situation developer should also preserve primary key or something from old system to new system 
to be able to link old records with new ones. Primary key from old system is the easiest way to keep track 
(if we are talking about SQL database).
Also possible to make hash (like sha256) from old row content to get unique row fingerprint and to save it with 
copied row in new system. In case of inconsistent there is possibility to go throw all old rows , 
to calculate hash of each row and to compare hash with new records to find diff.

I personally vote for saving just primary keys and keeping DB schemas to have a links with old records.  
